#include <Arduino.h>
#include <BluetoothSerial.h>

BluetoothSerial SerialBT;

char buffer[256];
uint8_t dataArrived = 0; 

void callback(esp_spp_cb_event_t event, esp_spp_cb_param_t *param){
  
  // When connected.
  if(event == ESP_SPP_SRV_OPEN_EVT){
    Serial.println("Client Connected");
  }

  if(event == ESP_SPP_DATA_IND_EVT) {
    int data = '\0';
    uint8_t counter = 0;
    while (1)
    {
      data = SerialBT.read();

      if(data == -1)
        return;

      // If it reached the buffers size place an end character at the end, and returns.
      if(counter == 255) {
        buffer[255] = '\0';
        dataArrived = 1;
        return;
      }

      // When the new line character arrives it is the end of the datastream.
      if(data == '\n') {
        buffer[counter] = '\0';
        dataArrived = 1;
        return;
      } else {
        buffer[counter] = data;
      }
      
      counter++;
    }
    
  }
}
 

void setup() {
  Serial.begin(9600);
  SerialBT.register_callback(callback);
    if(!SerialBT.begin("ESP32")){
    Serial.println("An error occurred initializing Bluetooth");
  }else{
    Serial.println("Bluetooth initialized");
  }
}

void loop() {
   if(dataArrived) {
     dataArrived = 0;
     Serial.print("Data arrived: ");
     Serial.println(buffer);
   }
}
